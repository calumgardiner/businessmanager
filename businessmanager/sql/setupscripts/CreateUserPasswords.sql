use BusinessManager;
CREATE TABLE UserPasswords(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	password_hash CHAR(60) NOT NULL,
	change_date DATETIME NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(id)
)
