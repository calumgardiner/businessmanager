use BusinessManager;
-- Create user login audit

CREATE TABLE UserLoginAudit(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	ip_address VARCHAR(30) NOT NULL,
	login_date DATETIME NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(id)
)