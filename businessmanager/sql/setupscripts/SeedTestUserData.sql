use BusinessManager;
-- DELETE FROM UserPasswords WHERE id > 0; DELETE FROM Users WHERE id > 0
-- Insert a test user
-- Calum Gardiner test user
INSERT INTO Users (forename, surname, create_date, email, is_deleted) 
	VALUES ('Calum', 'Gardiner', current_timestamp, 'calumg89@gmail.com', 0);
SET @UserId = LAST_INSERT_ID();

INSERT INTO Users (forename, surname, create_date, email, is_deleted)
	VALUES('Bruce', 'Craig', current_timestamp, 'bcraig@astins.co.uk', 0);
SET @User2Id = last_insert_id();

-- Set the password up
INSERT INTO UserPasswords (user_id, password_hash, change_date) 
	VALUES (@UserId, '$2a$10$H8dQht8.ztgUvZny.usvk.XIgvGRQNZFovA.IewmgZnY7895rFl..', '2015-09-16 22:37:06');

-- Set up an account
INSERT INTO Accounts(company_name, create_date, is_deleted) 
	VALUES ('Computers Inc.', current_timestamp, 0);
SET @AccountId = LAST_INSERT_ID();

-- Second account for testing
INSERT INTO Accounts(company_name, create_date, is_deleted)
	VALUES ('Just', current_timestamp, 0);
SET @Account2Id = LAST_INSERT_ID();

INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date) 
	VALUES (@AccountId, @UserId, 1, 0, current_timestamp);
    
INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date) 
	VALUES (@AccountId, @User2Id, 1, 0, current_timestamp);

INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date)
	VALUES(@Account2Id, @User2Id, 0, 0, current_timestamp);

INSERT INTO UserLoginAudit(user_id, ip_address, login_date)
	VALUES (@UserId, '192.168.0.1', CURRENT_TIMESTAMP);
