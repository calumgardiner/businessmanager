-- ########################################
-- Rebuild the database schema from scratch
DROP DATABASE IF EXISTS BusinessManager;
CREATE DATABASE BusinessManager;
use BusinessManager;

-- User table
CREATE TABLE Users(
	id 				INT PRIMARY KEY NOT NULL 	AUTO_INCREMENT,
	forename 		VARCHAR(30) 	NOT NULL,
	surname 		VARCHAR(30) 	NOT NULL,
	create_date 	DATETIME 		NOT NULL,
	email			VARCHAR(100)	NOT NULL,
	is_deleted		BIT 			NOT NULL
);
ALTER TABLE Users ADD CONSTRAINT UC_Users_Email UNIQUE (email);

-- Need to create table which links users to an account. alter
-- Each account will have its own data structures. This should allow us more control over what happens
-- And more security.
CREATE TABLE Accounts (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	company_name VARCHAR(50) NOT NULL,
	create_date DATETIME NOT NULL,
	is_deleted BIT NOT NULL
);

ALTER TABLE Accounts ADD CONSTRAINT UC_Company_Name UNIQUE (company_name);

CREATE TABLE UserAccounts (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	account_id INT NOT NULL,
	user_id INT NOT NULL,
	is_admin BIT NOT NULL,
	is_deleted BIT NOT NULL,
	change_date DATETIME NOT NULL,
	FOREIGN KEY (account_id) REFERENCES Accounts(id),
	FOREIGN KEY (user_id) REFERENCES Users(id)
);
ALTER TABLE UserAccounts ADD CONSTRAINT UC_Account_User UNIQUE (account_id, user_id);

-- Sessions
CREATE TABLE UserSessions (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	session_key VARCHAR(60) NOT NULL,
	session_start DATETIME NOT NULL,
	last_session_poll DATETIME,
	session_end DATETIME,
	FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Passwords
CREATE TABLE UserPasswords(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	password_hash CHAR(60) NOT NULL,
	change_date DATETIME NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Login Audit
-- Create user login audit, audits logins and invalid attempts to stop brute forcing
CREATE TABLE UserLoginAudit(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	ip_address VARCHAR(30) NOT NULL,
	login_date DATETIME NOT NULL,
	valid_login BIT NOT NULL,
	acknowledged BIT NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Request audit where each request will be audited, provides usage statistics
-- Can be used as per the login audit to stop brute forcing of session keys
CREATE TABLE RequestAudit(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ip_address VARCHAR(30) NOT NULL,
	request_date DATETIME NOT NULL,
    valid_session BIT NOT NULL,
    acknowledged BIT NOT NULL,
    request_details VARCHAR(1000) -- contains info about which request/if user details were present
								  -- its not possible to FK to user table as if the user object was
								  -- not provided we still want to log invalid requests
);

-- ################
-- Insert test data
INSERT INTO Users (forename, surname, create_date, email, is_deleted) 
	VALUES ('Calum', 'Gardiner', current_timestamp, 'calumg89@gmail.com', 0);
SET @UserId = LAST_INSERT_ID();

INSERT INTO Users (forename, surname, create_date, email, is_deleted)
	VALUES('Bruce', 'Craig', current_timestamp, 'bcraig@astins.co.uk', 0);
SET @User2Id = last_insert_id();

-- Set the password up
INSERT INTO UserPasswords (user_id, password_hash, change_date) 
	VALUES (@UserId, '$2a$10$H8dQht8.ztgUvZny.usvk.XIgvGRQNZFovA.IewmgZnY7895rFl..', '2015-09-16 22:37:06');

-- Set up an account
INSERT INTO Accounts(company_name, create_date, is_deleted) 
	VALUES ('Computers Inc.', current_timestamp, 0);
SET @AccountId = LAST_INSERT_ID();

-- Second account for testing
INSERT INTO Accounts(company_name, create_date, is_deleted)
	VALUES ('Just', current_timestamp, 0);
SET @Account2Id = LAST_INSERT_ID();

INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date) 
	VALUES (@AccountId, @UserId, 1, 0, current_timestamp);
    
INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date) 
	VALUES (@AccountId, @User2Id, 1, 0, current_timestamp);

INSERT INTO UserAccounts(account_id, user_id, is_admin, is_deleted, change_date)
	VALUES(@Account2Id, @User2Id, 0, 0, current_timestamp);

INSERT INTO UserLoginAudit(user_id, ip_address, login_date, valid_login, acknowledged)
	VALUES (@UserId, '192.168.0.1', CURRENT_TIMESTAMP, 1, 0);
    
