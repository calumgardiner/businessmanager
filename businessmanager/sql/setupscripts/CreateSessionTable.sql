use BusinessManager;

CREATE TABLE UserSessions (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	session_key VARCHAR(60) NOT NULL,
	session_start DATETIME NOT NULL,
	last_session_poll DATETIME,
	session_end DATETIME,
	FOREIGN KEY (user_id) REFERENCES Users(id)
);