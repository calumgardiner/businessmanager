use BusinessManager;
CREATE TABLE Users(
	id 				INT PRIMARY KEY NOT NULL 	AUTO_INCREMENT,
	forename 		VARCHAR(30) 	NOT NULL,
	surname 		VARCHAR(30) 	NOT NULL,
	create_date 	DATETIME 		NOT NULL,
	email			VARCHAR(100)	NOT NULL,
	is_deleted		BIT 			NOT NULL
);

ALTER TABLE Users ADD CONSTRAINT UC_Users_Email UNIQUE (email);
