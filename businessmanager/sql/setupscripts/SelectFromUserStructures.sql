use BusinessManager;

SELECT 
    *
FROM
    Users users
        LEFT OUTER JOIN
    UserPasswords passwords ON passwords.user_id = users.id
        LEFT OUTER JOIN
    UserAccounts useraccount ON useraccount.user_id = users.id
        LEFT OUTER JOIN
    Accounts account ON account.id = useraccount.account_id
        LEFT OUTER JOIN
    UserLoginAudit logins ON logins.user_id = users.id
WHERE
    users.is_deleted = 0
        AND account.is_deleted = 0