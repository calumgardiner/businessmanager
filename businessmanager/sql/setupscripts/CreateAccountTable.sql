use BusinessManager;
-- Need to create table which links users to an account. alter
-- Each account will have its own data structures. This should allow us more control over what happens
-- And more security.
CREATE TABLE Accounts (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	company_name VARCHAR(50) NOT NULL,
	create_date DATETIME NOT NULL,
	is_deleted BIT NOT NULL
);

ALTER TABLE Accounts ADD CONSTRAINT UC_Company_Name UNIQUE (company_name);

CREATE TABLE UserAccounts (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	account_id INT NOT NULL,
	user_id INT NOT NULL,
	is_admin BIT NOT NULL,
	is_deleted BIT NOT NULL,
	change_date DATETIME NOT NULL,
	FOREIGN KEY (account_id) REFERENCES Accounts(id),
	FOREIGN KEY (user_id) REFERENCES Users(id)
);
ALTER TABLE UserAccounts ADD CONSTRAINT UC_Account_User UNIQUE (account_id, user_id);