package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.central.util.HBUtils;
import io.calumg.businessmanager.central.util.SessionGenerator;
import io.calumg.businessmanager.common.dto.User;
import io.calumg.businessmanager.common.dto.UserSession;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class SessionDAO extends BusinessManagerDAO {

	private Logger log = LogManager.getLogger(SessionDAO.class);
	private SessionFactory factory;

	public SessionDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public UserSession createSession(User user) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			closeUserSessions(user);
			UserSession userSession = new UserSession();
			userSession.setSessionStart(new Date());
			userSession.setUser(user);
			userSession.setSessionKey(SessionGenerator.generateKey());
			while (!isUniqueSession(userSession.getSessionKey())) {
				userSession.setSessionKey(SessionGenerator.generateKey());
			}
			tx = session.beginTransaction();
			Integer sessionId = (Integer) session.save(userSession);
			userSession.setId(sessionId);
			if (commit) tx.commit();
			return userSession;
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public void closeSession(UserSession userSession) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			log.debug("Closing session : " + userSession.getUser().getEmail()
					+ " " + userSession.getSessionKey());
			userSession.setSessionEnd(new Date());
			tx = session.beginTransaction();
			session.update(userSession);
			if (commit) tx.commit();
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public void closeUserSessions(User user) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			log.debug("Closing current sessions for user : " + user.getEmail());
			Criteria cr = session.createCriteria(UserSession.class)
					.add(Restrictions.eq("user", user))
					.add(Restrictions.isNull("sessionEnd"));
			ScrollableResults openUserSessions = cr.scroll();
			Date now = new Date(); // create date once
			int count = 0;
			while (openUserSessions.next()) {
				UserSession userSession = (UserSession) openUserSessions.get(0);
				userSession.setSessionEnd(now);
				session.saveOrUpdate(userSession);
				if (++count % 100 == 0) {
					session.flush();
					session.clear();
				}
			}
			if (commit) tx.commit();
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public boolean isUniqueSession(String sessionKey) throws Exception {
		Session session = factory.openSession();
		try {
			Criteria cr = session.createCriteria(UserSession.class)
					.add(Restrictions.eq("sessionKey", sessionKey))
					.setProjection(Projections.rowCount());
			Long sessionCount = (Long) cr.uniqueResult();
			log.debug("Sessions already assigned this key: " + sessionCount);
			return (sessionCount == 0l);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.calumg.businessmanager.central.dao.BusinessManagerDAO#getCommit()
	 */
	public Boolean isCommiting() {
		return commit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.calumg.businessmanager.central.dao.BusinessManagerDAO#setCommit(java.
	 * lang.Boolean)
	 */
	public void setCommit(Boolean commit) {
		this.commit = commit;
	}

}
