package io.calumg.businessmanager.central.util;

import org.hibernate.Transaction;

/**
 * Static utilities for Hibernate.
 * 
 * @author Calum Gardiner
 *
 */
public class HBUtils {

	/**
	 * Attempt to rollback the transaction. Fail silently.
	 * 
	 * @param tx
	 */
	public static void rollbackTransaction(Transaction tx) {
		try {
			tx.rollback();
		} catch (Exception e) {}
	}

}
