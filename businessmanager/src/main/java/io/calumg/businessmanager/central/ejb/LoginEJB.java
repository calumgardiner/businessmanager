package io.calumg.businessmanager.central.ejb;

import io.calumg.businessmanager.central.dao.SessionDAO;
import io.calumg.businessmanager.central.dao.UserDAO;
import io.calumg.businessmanager.common.businessif.LoginIF;
import io.calumg.businessmanager.common.dto.LoginAttempt;
import io.calumg.businessmanager.common.dto.User;
import io.calumg.businessmanager.common.dto.UserSession;
import io.calumg.businessmanager.common.util.LoginAttemptUtil;

import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Business Logic EJB for login actions. Used locally on the server to provide
 * the dumb web services with the business logic.
 * 
 * @author Calum Gardiner
 *
 */
@Stateless
public class LoginEJB implements LoginIF {

	private Logger log = LogManager.getLogger(LoginEJB.class);
	private UserDAO userDAO;
	private SessionDAO sessionDAO;
	private SessionFactory factory;

	public LoginEJB() {
		this.factory = new Configuration().configure().buildSessionFactory();
		this.userDAO = new UserDAO(factory);
		this.sessionDAO = new SessionDAO(factory);
	}

	@Override
	public LoginAttempt login(String email, String password) throws Exception {
		try {
			// TODO check attempts made
			User user = userDAO.getUserFromEmail(email);
			if (user == null) return LoginAttemptUtil
					.getInvalidCredentialsResponse();
			// Correct credentials
			UserSession session = sessionDAO.createSession(user);
			return LoginAttemptUtil.getLoginSuccessfulResponse(session);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}

	@Override
	public void logout(UserSession session) throws Exception {
		try {
			this.sessionDAO.closeSession(session);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}

}
