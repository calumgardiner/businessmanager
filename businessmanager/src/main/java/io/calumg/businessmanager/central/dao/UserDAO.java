package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.central.util.HBUtils;
import io.calumg.businessmanager.common.dto.Password;
import io.calumg.businessmanager.common.dto.User;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.mindrot.jbcrypt.BCrypt;

/**
 * UserDAO, provides data access / writing for User related data.
 * 
 * @author Calum Gardiner
 *
 */
public class UserDAO extends BusinessManagerDAO {

	private Logger log = LogManager.getLogger(UserDAO.class);
	private SessionFactory factory;

	public UserDAO(SessionFactory factory) {
		this.factory = factory;
	}

	/**
	 * Create a user with the details specified.
	 * 
	 * @param user
	 * @return the id for the created user
	 * @throws Exception
	 */
	public Integer createUser(User user) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			user.setIsDeleted(Boolean.FALSE);
			tx = session.beginTransaction();
			Integer userId = (Integer) session.save(user);
			if (commit) tx.commit();
			return userId;
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * Get the user object for the login email specified.
	 * 
	 * @param email
	 * @return the User for this email if exists
	 * @throws Exception
	 */
	public User getUserFromEmail(String email) throws Exception {
		Session session = factory.openSession();
		try {
			Criteria cr = session.createCriteria(User.class)
					.add(Restrictions.eq("email", email))
					.add(Restrictions.eq("isDeleted", false)).setMaxResults(1);
			User user = (User) cr.uniqueResult();
			return user;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * Save the specified password for the user.
	 * 
	 * @param password
	 * @return the id for the saved password
	 * @throws Exception
	 */
	public Integer savePassword(Password password) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			String unhashedPass = password.getPassword();
			password.setPassword(BCrypt.hashpw(unhashedPass, BCrypt.gensalt()));
			tx = session.beginTransaction();
			Integer passwordId = (Integer) session.save(password);
			if (commit) tx.commit();
			return passwordId;
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * Get the latest password saved by the user.
	 * 
	 * @param user
	 * @return the password object
	 * @throws Exception
	 */
	public Password getPassword(User user) throws Exception {
		Session session = factory.openSession();
		try {
			Criteria cr = session.createCriteria(Password.class)
					.add(Restrictions.eq("user", user))
					.addOrder(Order.desc("changeDate")).setMaxResults(1);
			Password password = (Password) cr.uniqueResult();
			return password;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

}
