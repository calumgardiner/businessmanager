package io.calumg.businessmanager.central.util;

import java.math.BigInteger;
import java.security.SecureRandom;

public final class SessionGenerator {

	private static final SecureRandom random = new SecureRandom();

	public static String generateKey() {
		return new BigInteger(300, random).toString(32);
	}

}
