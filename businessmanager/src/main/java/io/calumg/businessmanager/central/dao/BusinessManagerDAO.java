package io.calumg.businessmanager.central.dao;

/**
 * Base interface for all DAO's in this project. Defines the required behaviour
 * for these DAO's.
 * 
 * @author Caalum Gardiner
 *
 */
public abstract class BusinessManagerDAO {

	protected Boolean commit = true; // Commit changes by default

	/**
	 * Get the commit status for this DAO instance.
	 * 
	 * @return true if commit is enabled.
	 */
	public Boolean isCommiting() {
		return commit;
	}

	/**
	 * Set the commit status for this DAO.
	 * 
	 * @param commit
	 */
	public void setCommit(Boolean commit) {
		this.commit = commit;
	}

}
