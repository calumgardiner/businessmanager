package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.central.util.HBUtils;
import io.calumg.businessmanager.common.dto.Account;
import io.calumg.businessmanager.common.dto.User;
import io.calumg.businessmanager.common.dto.UserAccount;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * Database read write for Accounts and there relationship with Users.
 * 
 * @author Calum Gardiner
 *
 */
public class AccountDAO extends BusinessManagerDAO {

	private Logger log = LogManager.getLogger(AccountDAO.class);
	private SessionFactory factory;

	public AccountDAO(SessionFactory factory) {
		this.factory = factory;
	}

	/**
	 * Create an account with the details specified.
	 * 
	 * @param user
	 * @return the id for the created account
	 * @throws Exception
	 */
	public Integer createAccount(Account account) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			account.isDeleted(false);
			Integer accountId = (Integer) session.save(account);
			if (commit) tx.commit();
			return accountId;
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public UserAccount createUserAccount(User user, Account account,
			Boolean isAdmin) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			UserAccount userAccount = new UserAccount();
			userAccount.setAccount(account);
			userAccount.setUser(user);
			userAccount.setChangeDate(new Date());
			userAccount.setIsAdmin(isAdmin);
			userAccount.isDeleted(false);
			tx = session.beginTransaction();
			Integer id = (Integer) session.save(userAccount);
			userAccount.setId(id);
			if (commit) tx.commit();
			return userAccount;
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public void deleteAccount(Account account) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			account.isDeleted(true);
			tx = session.beginTransaction();
			session.update(account);
			if (commit) tx.commit();
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public void deleteUserAccount(UserAccount account) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			account.isDeleted(true);
			tx = session.beginTransaction();
			session.update(account);
			if (commit) tx.commit();
		} catch (Exception e) {
			HBUtils.rollbackTransaction(tx);
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public List<UserAccount> getUserAccountsForUser(User user) throws Exception {
		Session session = factory.openSession();
		try {
			Criteria cr = session.createCriteria(UserAccount.class)
					.add(Restrictions.eq("user", user))
					.add(Restrictions.eq("isDeleted", false));
			List<UserAccount> accounts = new ArrayList<>();
			ScrollableResults results = cr.scroll();
			while (results.next()) {
				accounts.add((UserAccount) results.get(0));
			}
			return accounts;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}

	public List<UserAccount> getUserAccountsForAccount(Account account)
			throws Exception {
		Session session = factory.openSession();
		try {
			Criteria cr = session.createCriteria(UserAccount.class)
					.add(Restrictions.eq("account", account))
					.add(Restrictions.eq("isDeleted", false));
			List<UserAccount> accounts = new ArrayList<>();
			ScrollableResults results = cr.scroll();
			while (results.next()) {
				accounts.add((UserAccount) results.get(0));
			}
			return accounts;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		} finally {
			session.close();
		}
	}
}
