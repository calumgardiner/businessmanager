package io.calumg.businessmanager.common.businessif;

import io.calumg.businessmanager.common.dto.LoginAttempt;
import io.calumg.businessmanager.common.dto.UserSession;

import javax.ejb.Local;

@Local
public interface LoginIF {

	public LoginAttempt login(String email, String password) throws Exception;

	public void logout(UserSession session) throws Exception;
}
