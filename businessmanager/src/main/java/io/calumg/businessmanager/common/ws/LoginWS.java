package io.calumg.businessmanager.common.ws;

import io.calumg.businessmanager.common.businessif.LoginIF;
import io.calumg.businessmanager.common.dto.LoginAttempt;
import io.calumg.businessmanager.common.dto.UserSession;

import javax.ejb.EJB;
import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebService
public class LoginWS {

	private Logger log = LogManager.getLogger(LoginWS.class);

	@EJB
	private LoginIF loginEJB;

	public LoginAttempt login(String email, String password) throws Exception {
		try {
			return loginEJB.login(email, password);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}

	public void logout(UserSession session) throws Exception {
		try {
			loginEJB.logout(session);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}
}
