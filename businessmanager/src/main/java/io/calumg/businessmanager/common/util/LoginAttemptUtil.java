package io.calumg.businessmanager.common.util;

import io.calumg.businessmanager.common.dto.LoginAttempt;
import io.calumg.businessmanager.common.dto.LoginResponseCode;
import io.calumg.businessmanager.common.dto.UserSession;

public class LoginAttemptUtil {

	public static LoginAttempt getInvalidCredentialsResponse() {
		LoginAttempt response = new LoginAttempt();
		response.setResponseCode(LoginResponseCode.INVALID_CREDENTIALS);
		return response;
	}

	public static LoginAttempt getLoginSuccessfulResponse(UserSession session) {
		return new LoginAttempt(session);
	}

	public static LoginAttempt getTooManyAttemptsResponse() {
		LoginAttempt response = new LoginAttempt();
		response.setResponseCode(LoginResponseCode.TOO_MANY_ATTEMPTS);
		return response;
	}

}
