package io.calumg.businessmanager.common.dto;

public enum LoginResponseCode {

	SUCCESS, INVALID_CREDENTIALS, TOO_MANY_ATTEMPTS;

}
