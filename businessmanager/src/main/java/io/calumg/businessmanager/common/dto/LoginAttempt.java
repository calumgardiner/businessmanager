package io.calumg.businessmanager.common.dto;

public class LoginAttempt {

	private UserSession userSession;
	private LoginResponseCode responseCode;

	public LoginAttempt() {
	}

	public LoginAttempt(UserSession userSession) {
		this.userSession = userSession;
		this.responseCode = LoginResponseCode.SUCCESS;
	}

	public UserSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	public LoginResponseCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(LoginResponseCode responseCode) {
		this.responseCode = responseCode;
	}

}
