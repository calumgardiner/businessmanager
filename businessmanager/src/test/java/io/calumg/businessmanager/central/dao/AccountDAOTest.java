package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.common.dto.Account;
import io.calumg.businessmanager.common.dto.User;
import io.calumg.businessmanager.common.dto.UserAccount;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testing for {@link AccountDAO}
 * 
 * @author Calum Gardiner
 *
 */
public class AccountDAOTest {

	private Logger log = LogManager.getLogger(AccountDAOTest.class);

	private AccountDAO dao;
	private SessionFactory factory;

	@Before
	public void initialSetup() {
		factory = new Configuration().configure().buildSessionFactory();
		dao = new AccountDAO(factory);
		dao.setCommit(false);
	}

	@Test
	public void testCreateAccount() throws Exception {
		try {
			Account account = new Account();
			account.setCompanyName("The Business");
			account.setCreateDate(new Date());
			account.isDeleted(false);
			Integer accountId = dao.createAccount(account);
			Assert.assertNotNull(accountId);
			log.debug(accountId);
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@Test
	public void testCreateUserAccount() throws Exception {
		try {
			Account account = new Account();
			account.setCompanyName("Just");
			account.setCreateDate(new Date());
			account.isDeleted(Boolean.FALSE);
			account.setId(2);
			User newUser = new User();
			newUser.setCreateDate(new Date());
			newUser.setEmail("calumg89@gmail.com");
			newUser.setForename("Calum");
			newUser.setSurname("Gardiner");
			newUser.setId(1);
			UserAccount acc = dao.createUserAccount(newUser, account, true);
			Assert.assertNotNull(acc.getId());
			log.debug("User Associated with account: " + acc.getId());
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@After
	public void shutDown() {
		factory.close();
	}
}
