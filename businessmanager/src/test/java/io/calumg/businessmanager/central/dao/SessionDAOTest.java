package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.common.dto.User;
import io.calumg.businessmanager.common.dto.UserSession;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testing for {@link SessionDAO}
 * 
 * @author Calum Gardiner
 *
 */
public class SessionDAOTest {

	private Logger log = LogManager.getLogger(SessionDAOTest.class);

	private SessionDAO dao;
	private SessionFactory factory;

	@Before
	public void initialSetup() {
		factory = new Configuration().configure().buildSessionFactory();
		dao = new SessionDAO(factory);
		dao.setCommit(false);
	}

	@Test
	public void testCreateSession() throws Exception {
		try {
			User newUser = new User();
			newUser.setCreateDate(new Date());
			newUser.setEmail("calumg89@gmail.com");
			newUser.setForename("Calum");
			newUser.setSurname("Gardiner");
			newUser.setId(1);
			newUser.setIsDeleted(false);
			UserSession session = dao.createSession(newUser);
			log.debug("Session Created: " + session.getId() + " "
					+ session.getSessionKey());
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@After
	public void shutDown() {
		factory.close();
	}
}
