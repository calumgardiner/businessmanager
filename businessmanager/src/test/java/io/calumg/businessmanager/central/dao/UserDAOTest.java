package io.calumg.businessmanager.central.dao;

import io.calumg.businessmanager.common.dto.Password;
import io.calumg.businessmanager.common.dto.User;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Testing for {@link UserDAO}
 * 
 * @author Calum Gardiner
 *
 */
public class UserDAOTest {

	private Logger log = LogManager.getLogger(UserDAOTest.class);

	private UserDAO dao;
	private SessionFactory factory;

	@Before
	public void initialSetup() {
		factory = new Configuration().configure().buildSessionFactory();
		dao = new UserDAO(factory);
		dao.setCommit(false);
	}
	
	@Test
	public void testGetUserForEmail() {
		try {
			String email = "calumg89@gmail.com";
			User user = dao.getUserFromEmail(email);
			log.debug("Returned user id : " + user.getId() + " for user : "
					+ email);
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@Test
	public void testCreateUser() {
		try {
			User newUser = new User();
			newUser.setCreateDate(new Date());
			newUser.setEmail("useremail@email.com");
			newUser.setForename("John");
			newUser.setSurname("Smith");
			Integer savedUser = dao.createUser(newUser);
			log.debug("Saved user id : " + savedUser);
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@Test
	public void testSavePassword() {
		try {
			User newUser = new User();
			newUser.setCreateDate(new Date());
			newUser.setEmail("calumg89@gmail.com");
			newUser.setForename("Calum");
			newUser.setSurname("Gardiner");
			newUser.setId(1);
			newUser.setIsDeleted(false);
			Password password = new Password();
			password.setUser(newUser);
			password.setChangeDate(new Date());
			password.setPassword("Test");
			Integer savedPassword = dao.savePassword(password);
			log.debug("Saved password id : " + savedPassword);
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}

	}

	@Test
	public void testGetPassword() {
		try {
			User newUser = new User();
			newUser.setCreateDate(new Date());
			newUser.setEmail("calumg89@gmail.com");
			newUser.setForename("Calum");
			newUser.setSurname("Gardiner");
			newUser.setId(1);
			newUser.setIsDeleted(false);
			Password password = dao.getPassword(newUser);
			log.debug("Returned password : " + password.getPassword());
			Assert.assertEquals(
					"$2a$10$H8dQht8.ztgUvZny.usvk.XIgvGRQNZFovA.IewmgZnY7895rFl..",
					password.getPassword());
		} catch (Exception e) {
			log.error(e);
			Assert.fail("Exception thrown: " + e.getMessage());
		}
	}

	@After
	public void shutDown() {
		factory.close();
	}
}
